/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.cdi;

import cas.entity.Adminuser;
import cas.entity.Attendance;
import cas.entity.Course;
import cas.entity.Slot;
import cas.entity.Student;
import cas.entity.Teacher;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Named
@SessionScoped
public class DataBB implements Serializable {

    // ==============================
    // =          Attributes        =
    // ==============================
    // #######################
    // #   Selected entity   #
    // #######################
    private Teacher selectedTeacher;
    private Student selectedStudent;
    private Course selectedCourse;
    private Slot selectedSlot;
    private Attendance selectedAttendance;
    private List<Student> selectedStudentList;
    private List<Slot> selectedSlotList;
    private List<Adminuser> selectedAdminuserList;
    private int curYear;
    private Integer selectedResponsibleTeacherId;

    // ==================================
    // =          Constructor           =
    // ==================================
    public DataBB() {
        this.selectedStudentList = new CopyOnWriteArrayList<>();
        this.selectedSlotList = new CopyOnWriteArrayList<>();
        this.selectedAdminuserList = new CopyOnWriteArrayList<>();
        this.curYear = LocalDate.now().getYear();

    }

    // =================================
    // =          Public methods       =
    // =================================
    // ==================================
    // =          Getter & Setter       =
    // ==================================
    public Teacher getSelectedTeacher() {
        return selectedTeacher;
    }

    public void setSelectedTeacher(Teacher selectedTeacher) {
        this.selectedTeacher = selectedTeacher;
    }

    public Student getSelectedStudent() {
        return selectedStudent;
    }

    public void setSelectedStudent(Student selectedStudent) {
        this.selectedStudent = selectedStudent;
    }

    public Course getSelectedCourse() {
        return selectedCourse;
    }

    public void setSelectedCourse(Course selectedCourse) {
        this.selectedCourse = selectedCourse;
    }

    public Slot getSelectedSlot() {
        return selectedSlot;
    }

    public void setSelectedSlot(Slot selectedSlot) {
        this.selectedSlot = selectedSlot;
    }

    public Attendance getSelectedAttendance() {
        return selectedAttendance;
    }

    public void setSelectedAttendance(Attendance selectedAttendance) {
        this.selectedAttendance = selectedAttendance;
    }

    public List<Student> getSelectedStudentList() {
        return selectedStudentList;
    }

    public void setSelectedStudentList(List<Student> selectedStudentList) {
        this.selectedStudentList = selectedStudentList;
    }

    public List<Slot> getSelectedSlotList() {
        return selectedSlotList;
    }

    public void setSelectedSlotList(List<Slot> selectedSlotList) {
        this.selectedSlotList = selectedSlotList;
    }

    public List<Adminuser> getSelectedAdminuserList() {
        return selectedAdminuserList;
    }

    public void setSelectedAdminuserList(List<Adminuser> selectedAdminuserList) {
        this.selectedAdminuserList = selectedAdminuserList;
    }

    public int getCurYear() {
        return curYear;
    }

    public void setCurYear(int curYear) {
        this.curYear = curYear;
    }

    public Integer getSelectedResponsibleTeacherId() {
        return selectedResponsibleTeacherId;
    }

    public void setSelectedResponsibleTeacherId(Integer selectedResponsibleTeacherId) {
        this.selectedResponsibleTeacherId = selectedResponsibleTeacherId;
    }

}
