/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.cdi;

import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Named
@RequestScoped
public class AuthorizationBB implements Serializable {
    // ==============================
    // =          Attributes        =
    // ==============================

    private String username;
    private String password;

    // ==================================
    // =          Constructor           =
    // ==================================
    public AuthorizationBB() {
    }

    // =================================
    // =          Public methods       =
    // =================================
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        try {
            request.login(getUsername(), getPassword());
            return "/view/secure/start.xhtml?faces-redirect=true";
        } catch (ServletException ex) {
            return "/view/secure/login/loginerror.xhtml?faces-redirect=true";
        }
    }

    public String logout() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        try {
            request.logout();
        } catch (ServletException ex) {
            Logger.getLogger(AuthorizationBB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/view/secure/login/login.xhtml?faces-redirect=true";
    }

    public void onPageLoad() throws ServletException, IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            try {
                StringBuilder redirectURL = new StringBuilder(request.getContextPath());
                redirectURL.append("/view/secure/start.xhtml");
                FacesContext.getCurrentInstance().getExternalContext().redirect(redirectURL.toString());
            } catch (IOException ex) {
                request.logout();
            }
        }
    }

    // ==================================
    // =          Getter & Setter       =
    // ==================================
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
