/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.cdi;

import cas.entity.Adminuser;
import cas.entity.Attendance;
import cas.entity.Course;
import cas.entity.Slot;
import cas.entity.Student;
import cas.entity.Teacher;
import cas.logic.CasFactoryLocal;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Named
// TODO: need to check if "requestscoped" works or not -> change dependent???
@ViewScoped
//@RequestScoped
public class Controller implements Serializable {

    // ==============================
    // =          Attributes        =
    // ==============================
    // #######################
    // #        Admin        #
    // #######################
    private String username;
    private String password;

    // #######################
    // #       Teacher       #
    // #######################
    private String teacherFirstName;
    private String teacherLastName;

    // #######################
    // #       Student       #
    // #######################
    private String studentFirstName;
    private String studentLastName;
    private String studentSex;
    private Date dob;
    private String studentAddress;
    private String studentPostcode;
    private String studentPosttown;
    private String studentEMail;
    private String studentPhonenumber;

    // #######################
    // #     Registration    #
    // #######################
    private Integer registerStudentId;
    // #######################
    // #       Course        #
    // #######################
    private String courseName;
    private String courseLevel;
    private String courseLanguage;
    private Date startDate;
    private Date endDate;
    private int maxStudents;
    private Integer responsibleTeacherId;

    // #######################
    // #         Slot        #
    // #######################
    private Date startTime;
    private int duration;
    private String room;

    // #######################
    // #      Attendance     #
    // #######################
    private Integer studentId;
    private boolean attendanceStatus;

    // #######################
    // #  Data Backing bean  #
    // #######################
    @Inject
    private DataBB dataBb;

    // #######################
    // #   Buisiness logic   #
    // #######################
    @EJB
    private CasFactoryLocal casFactoryManager;

    // #######################
    // #        Others       #
    // #######################
    private String logininfo;

    // =================================
    // =          Public methods       =
    // =================================
    @PostConstruct
    public void init() {
        setLogininfo("You are not logged in.");
    }

    // #######################
    // #     Navigation      #
    // #######################
    public String goToLogin() {
        return "/view/secure/login/login.xhtml?faces-redirect=true";
    }

    public String check() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        if (request.getUserPrincipal().getName() != null) {
            setLogininfo("You are logged in as: " + request.getUserPrincipal().getName());
            return null;
        } else {
            return "/view/secure/login/login.xhtml?faces-redirect=true";
        }
    }

    public String backToStart() {
        return "/view/secure/start.xhtml?faces-redirect=true";
    }

    public String goToTeacherList() {
        return "/view/secure/teacher/teacher.xhtml?faces-redirect=true";
    }

    public String goToManageTeacher() {
        return "/view/secure/teacher/manageTeacher.xhthml?faces-redirect=true";
    }
//    public void goToManageTeacher() {
//        viewDialog("manageTeacher");
//    }

    public String goToStudentList() {
        return "/view/secure/student/student.xhtml?faces-redirect=true";
    }

    public String goToManageStudent() {
        return "/view/secure/student/manageStudent.xhtml?faces-redirect=true";
    }

    public String goToDetailStudent() {
        return "/view/secure/student/detailStudent.xhtml?faces-redirect=true";
    }

    public String goToCourseList() {
        return "/view/secure/course/course.xhtml?faces-redirect=true";
    }

    public String goToCourseInfo() {
        return "/view/secure/course/courseInfo.xhtml?faces-redirect=true";
    }

    public String goToRegisterStudent() {
        return "/view/secure/course/registerStudent.xhtml?faces-redirect=true";
    }

    public String goToManageCourse() {
        return "/view/secure/course/manageCourse.xhtml?faces-redirect=true";
    }
//    public void goToManageCourse() {
//        viewDialog("manageCourse");
//    }

    public String goToManageSchedule() {
        return "/view/secure/course/manageSchedule.xhtml?faces-redirect=true";
    }

    public String goToAttendanceRecord() {
        return "/view/secure/course/attendanceRecord.xhtml?faces-redirect=true";
    }

    public String goToManageAdmin() {
        return "/view/secure/admin/manageAdmin.xhtml?faces-redirect=true";
    }

    public void viewDialog(String destination) {
        Map<String, Object> options = new HashMap<>();
        options.put("draggable", false);
        options.put("resizable", false);
        options.put("modal", true);
//        options.put("closable", false);
        RequestContext.getCurrentInstance().openDialog(destination, options, null);
    }

    public void closeDialog(Object returnValue) {
        RequestContext.getCurrentInstance().closeDialog(returnValue);
    }

    public void handleReturn(SelectEvent event) {
//        Object returnObject = event.getObject();
    }

    // #######################
    // #   related Admin   #
    // #######################
    public String registerAdminuser() {
        if (!getCasFactoryManager().getAdminLogic().checkDuplicate(username)) {
//        try {
            getCasFactoryManager().getAdminLogic().createUserAndGroup(username, password, "admin"); // TODO: need to modify groupname when adding more roles
            resetValues();
            return goToManageAdmin();
        } else {
//        } catch (DatabaseException e) {

            FacesContext facesContext = FacesContext.getCurrentInstance();
//            facesContext.addMessage(null, new FacesMessage(e.toString()));
            facesContext.addMessage(null, new FacesMessage("Username is already used."));
            return null;
//        }
        }
    }

    public List<Adminuser> getAdminuserList() {
//        List<Adminuser> aduList = getCasFactoryManager().getAdminLogic().getAllAdminusers();
        List<Adminuser> aduList = getCasFactoryManager().getAdminLogic().getSortedAdminuserList();
        return aduList;
    }

    public String removeAdminusers() {
        for (Adminuser au : dataBb.getSelectedAdminuserList()) {
            getCasFactoryManager().getAdminLogic().removeUser(au.getUsername());
        }
//        dataBb.getSelectedAdminuserList().clear();
        resetValues();
        return goToManageAdmin();
    }

    // #######################
    // #   related Teacher   #
    // #######################
    public String registerTeacher() {
        Teacher nt = new Teacher();
        nt.setFirstname(teacherFirstName);
        nt.setLastname(teacherLastName);
        getCasFactoryManager().getTeacherLogic().addTeacher(nt);
        resetValues();
        return goToTeacherList();
    }

    public List<Teacher> getTeacherList() {
//        List<Teacher> tList = getCasFactoryManager().getTeacherLogic().getAllTeachers();
        List<Teacher> tList = getCasFactoryManager().getTeacherLogic().getSortedTeacherList();
        return tList;
    }

    public String showTeacherEdit(Integer id) {
        dataBb.setSelectedTeacher(getCasFactoryManager().getTeacherLogic().findTeacher(id));
//        dataBb.setSelectedTeacherId(id);
        return goToManageTeacher();
    }

    public String updateTeacher() {
        getCasFactoryManager().getTeacherLogic().updateTeacher(dataBb.getSelectedTeacher());
//        closeDialog(null);
//        getCasFactoryManager().getTeacherLogic().updateTeacher(getCasFactoryManager().getTeacherLogic().findTeacher(dataBb.getSelectedTeacherId()));
        return goToTeacherList();
    }

    public String deleteTeacher(Integer id) {
        // TODO: check if there is any responsible course
        if (!hasResponsibleCourse(id)) {
            getCasFactoryManager().getTeacherLogic().deleteTeacher(id);
            return goToTeacherList();
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage("This teacher is responsible for at least one course. Modify it before removing this teacher."));
            return null;
        }
    }

    // #######################
    // #   related Student   #
    // #######################
    public String registerStudent() {
//        Student nstu = new Student(studentFirstName, studentLastName, studentSex, dob, studentEMail, studentPhonenumber);
        Student nstu = new Student();
        nstu.setFirstname(studentFirstName);
        nstu.setLastname(studentLastName);
        nstu.setSex(studentSex);
        nstu.setDob(dob);
        nstu.setEmail(studentEMail);
        nstu.setPhonenr(studentPhonenumber);
        if (studentAddress.trim().length() > 0) {
            nstu.setAddress(studentAddress);
        }
        if (studentPostcode.trim().length() > 0) {
            nstu.setPostcode(studentPostcode);
        }
        if (studentPosttown.trim().length() > 0) {
            nstu.setPosttown(studentPosttown);
        }
        getCasFactoryManager().getStudentLogic().addStudent(nstu);
        resetValues();
        return goToStudentList();
    }

    public List<Student> getStudentList() {
//        List<Student> stuList = getCasFactoryManager().getStudentLogic().getAllStudents();
        List<Student> stuList = getCasFactoryManager().getStudentLogic().getSortedStudentList();
        return stuList;
    }

    public String showStudentEdit(Integer id) {
        dataBb.setSelectedStudent(getCasFactoryManager().getStudentLogic().findStudent(id));
        return goToManageStudent();
    }

    public String showDetailedInfo(Integer id) {
        dataBb.setSelectedStudent(getCasFactoryManager().getStudentLogic().findStudent(id));
        return goToDetailStudent();
    }

    public String updateStudent() {
        getCasFactoryManager().getStudentLogic().updateStudent(dataBb.getSelectedStudent());
        return goToStudentList();
    }

    public String deleteStudent(Integer id) {
        getCasFactoryManager().getStudentLogic().deleteStudent(id);
        return goToStudentList();
    }

    // #######################
    // #   related Course   #
    // #######################
    public String registerCourse() {
//        Course nc = new Course(courseName, courseLevel, courseLanguage, startDate, endDate, maxStudents);
        if (validateStartEndValue(startDate, endDate)) {
            Course nc = new Course();
            nc.setName(courseName);
            nc.setLevel(courseLevel);
            nc.setLanguage(courseLanguage);
            nc.setStart(startDate);
            nc.setEnd(endDate);
            nc.setMaxstudents(maxStudents);
            nc.setResponsible(getCasFactoryManager().getTeacherLogic().findTeacher(responsibleTeacherId));
            getCasFactoryManager().getCourseLogic().addCourse(nc);
            resetValues();
            return goToCourseList();
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage("Invalid: end date must be set after start date."));
            return null;
        }
    }

    public List<Course> getCourseList() {
//        List<Course> cList = getCasFactoryManager().getCourseLogic().getAllCourse();
        List<Course> cList = getCasFactoryManager().getCourseLogic().getSortedCourseList();
        return cList;
    }

    public String showCourseEdit(Integer id) {
        dataBb.setSelectedCourse(getCasFactoryManager().getCourseLogic().findCourse(id));
        return goToManageCourse();

    }

    public String updateCourse() {
        if (validateStartEndValue(dataBb.getSelectedCourse().getStart(), dataBb.getSelectedCourse().getEnd())) {
            dataBb.getSelectedCourse().setResponsible(getCasFactoryManager().getTeacherLogic().findTeacher(dataBb.getSelectedResponsibleTeacherId()));
            getCasFactoryManager().getCourseLogic().updateCourse(dataBb.getSelectedCourse());
            return goToCourseList();
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage("Invalid: end date must be set after start date."));
            return null;
        }
    }

    public String deleteCourse(Integer id) {
        getCasFactoryManager().getCourseLogic().deleteCourse(id);
        return goToCourseList();
    }

    public String showCourseInfo(Integer id) {
        dataBb.setSelectedCourse(getCasFactoryManager().getCourseLogic().findCourse(id));
        return goToCourseInfo();
    }

    public String showManageSchedule(Integer id) {
        dataBb.setSelectedCourse(getCasFactoryManager().getCourseLogic().findCourse(id));
        return goToManageSchedule();
    }

    public String showRegisterStudent(Integer id) {
        dataBb.setSelectedCourse(getCasFactoryManager().getCourseLogic().findCourse(id));
        return goToRegisterStudent();
    }

    public String addStudentToCourse() {
        if (calculateLeftAvailability() > 0) {
            Student regStudent = getCasFactoryManager().getStudentLogic().findStudent(registerStudentId);
            dataBb.getSelectedCourse().addStudent(regStudent);
            getCasFactoryManager().getCourseLogic().updateCourse(dataBb.getSelectedCourse());
            regStudent.addCourse(dataBb.getSelectedCourse());
            getCasFactoryManager().getStudentLogic().updateStudent(regStudent);
            // TODO: add attendance for each slot
            List<Slot> slotList = (List<Slot>) dataBb.getSelectedCourse().getSlotCollection();
            for (Slot s : slotList) {
                Attendance att = new Attendance();
                att.setSlot(s);
                att.setStudent(regStudent);
                att.setStatus(false);
                getCasFactoryManager().getAttendanceLogic().addAttendance(att);
            }
            resetValues();
            return goToRegisterStudent();
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage("No available place in this course."));
            return null;
        }
    }

    public void removeStudentFromCourse() {
        for (Student s : dataBb.getSelectedStudentList()) {
            dataBb.getSelectedCourse().removeStudent(s);
// TODO: remove att for removed student for each slot
            for (Attendance attstu : s.getAttendanceCollection()) {
                if (attstu.getStudent().getStudentId().equals(s.getStudentId())) {
//                    s.getAttendanceCollection().remove(attstu);
                    getCasFactoryManager().getAttendanceLogic().deleteAttendance(attstu.getAttendanceId());
                }
            }
//            getCasFactoryManager().getStudentLogic().updateStudent(s);
//            List<Slot> slotList = (List<Slot>) dataBb.getSelectedCourse().getSlotCollection();
//            for (Slot sl : slotList) {
//                for (Attendance att : sl.getAttendanceCollection()) {
//                    if (att.getStudent().getStudentId().equals(s.getStudentId())) {
////                        sl.getAttendanceCollection().remove(att);
//                        getCasFactoryManager().getAttendanceLogic().deleteAttendance(att.getAttendanceId());
//                        break;
//                    }
//                }
//            }
        }
        getCasFactoryManager().getCourseLogic().updateCourse(dataBb.getSelectedCourse());
        resetValues();
    }

    // #######################
    // #    related Slot     #
    // #######################
    public String registerSlot() {
//Slot ns = new Slot(studentId, startDate, 0, room);
        if (!isWithinCoursePeriod(startTime, duration)) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage("Slot must be within the course period."));
            return null;
        } else if (checkOverlap((List<Slot>) getCasFactoryManager().getSlotLogic().getAllSlot(), startTime, duration, room, dataBb.getSelectedCourse().getCourseId())) {
            Slot ns = new Slot();
            ns.setStartdate(startTime);
            ns.setDuration(duration);
            ns.setRoom(room);
            ns.setCourse(dataBb.getSelectedCourse());
            getCasFactoryManager().getSlotLogic().addSlot(ns);
            // TOTO: add attendance to each registered Student
            List<Student> studentList = (List<Student>) dataBb.getSelectedCourse().getStudentCollection();
            for (Student s : studentList) {
                Attendance att = new Attendance();
                att.setSlot(ns);
                att.setStudent(s);
                att.setStatus(false);
                getCasFactoryManager().getAttendanceLogic().addAttendance(att);
            }
            resetValues();
            return goToManageSchedule();
        } else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage(null, new FacesMessage("Overlapping existing slots."));
            return null;
        }
    }

    public List<Slot> getSlotList(Integer id) {
//        List<Slot> slotList = (List<Slot>) getCasFactoryManager().getCourseLogic().findCourse(dataBb.getSelectedCourse().getCourseId()).getSlotCollection();
        List<Slot> slotList = getCasFactoryManager().getSlotLogic().getSortedSlotList(id);
        return slotList;
    }

    public String removeSlotFromSchedule() {
        for (Slot s : dataBb.getSelectedSlotList()) {
            getCasFactoryManager().getSlotLogic().deleteSlot(s.getSlotId());
//            dataBb.getSelectedCourse().getSlotCollection().remove(s);
        }
//        dataBb.getSelectedCourse().getSlotCollection().removeAll(dataBb.getSelectedSlotList());
//        getCasFactoryManager().getCourseLogic().updateCourse(dataBb.getSelectedCourse());
//        Course c = getCasFactoryManager().getCourseLogic().findCourse(dataBb.getSelectedCourse().getCourseId());
//        dataBb.setSelectedCourse(c);
        resetValues();
        return goToManageSchedule();
    }

    // #######################
    // # related Attendance  #
    // #######################
    public String showAttendanceList(Integer id) {
        dataBb.setSelectedSlot(getCasFactoryManager().getSlotLogic().findSlot(id));
        return goToAttendanceRecord();
    }

    public List<Attendance> getAttendanceListBySlot(Integer id) {
        List<Attendance> attList = getCasFactoryManager().getAttendanceLogic().getSortedAttendanceListBySlot(id);
        return attList;
    }

    public List<Attendance> getAttendanceListByStudent(Integer id) {
        List<Attendance> attList = getCasFactoryManager().getAttendanceLogic().getSortedAttendanceListByStudent(id);
        return attList;
    }

    public String updateAttendanceList() {
        getCasFactoryManager().getSlotLogic().updateSlot(dataBb.getSelectedSlot());
        return goToCourseInfo();
    }

    // #######################
    // #        Others       #
    // #######################
    public void resetValues() {
        // register new teacher
        setTeacherFirstName("");
        setTeacherLastName("");
        // register new student
        setStudentFirstName("");
        setStudentLastName("");
        setStudentSex("");
        setStudentEMail("");
        setStudentPhonenumber("");
        setStudentAddress("");
        setStudentPostcode("");
        setStudentPosttown("");
        setDob(null);
        //register new course
        setCourseName("");
        setCourseLevel("");
        setCourseLanguage("");
        setStartDate(null);
        setEndDate(null);
        setMaxStudents(30);
//        course.setResponsibleTeacherIdx(0);
//        if (!dataRepository.getTeacherList().isEmpty()) {
//            course.setResponsibleTeacherId(dataRepository.getFirstTeacherId());
//        }
        // register new slot
        setStartTime(null);
        setDuration(0);
        setRoom("");

//        register new student to selected course
//        if (!dataRepository.getStudentList().isEmpty()) {
//            dataRepository.setSelectedStudent(dataRepository.getStudentById(dataRepository.getFirstStudentId()));
//        }
        setRegisterStudentId(1);

        // register new adminuser
        setUsername("");
        setPassword("");

        dataBb.getSelectedSlotList().clear();
        dataBb.getSelectedStudentList().clear();
        dataBb.getSelectedAdminuserList().clear();
    }

    public boolean hasResponsibleCourse(Integer id) {
        boolean hasResponsible = false;
        for (Course c : getCasFactoryManager().getCourseLogic().getAllCourse()) {
            if (c.getResponsible().getTeacherId().equals(id)) {
                hasResponsible = true;
                break;
            }
        }
        return hasResponsible;
    }

    public String getDateString(Date dt, int format) {// TODO: convert Date to String by LocalDate or LocalDateTime
//        String dString = null;
        LocalDateTime ldt = LocalDateTime.ofInstant(dt.toInstant(), ZoneId.systemDefault());
        DateTimeFormatter dFormat;
        // format: 0 = LocalDate, 1 = LocalDateTime
        if (format == 0) {
            dFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            return ldt.toLocalDate().format(dFormat);
        } else {
            dFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            return ldt.format(dFormat);
        }
    }

    public String getSlotEnd(Date dt, int duration) {
        LocalDateTime slotStart = LocalDateTime.ofInstant(dt.toInstant(), ZoneId.systemDefault());
        LocalDateTime slotEnd = slotStart.plusMinutes(duration);
        DateTimeFormatter dFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//        System.out.println(slotStart.format(dFormat) + ":" + slotEnd.format(dFormat));
        return slotEnd.format(dFormat);
    }

    public int calculateLeftAvailability() {
        Course c = getCasFactoryManager().getCourseLogic().findCourse(dataBb.getSelectedCourse().getCourseId());
        int leftAvailability = c.getMaxstudents() - c.getStudentCollection().size();
        return leftAvailability;
    }

    public boolean validateStartEndValue(Date startDate, Date endDate) {
        boolean validPeriod = true;
        if (LocalDateTime.ofInstant(endDate.toInstant(), ZoneId.systemDefault()).toLocalDate().compareTo(LocalDateTime.ofInstant(startDate.toInstant(), ZoneId.systemDefault()).toLocalDate()) < 0) {
            validPeriod = false;
        }
        return validPeriod;
    }

    public boolean isWithinCoursePeriod(Date startTime, int duration) {
        boolean isAcceptableSlot = true;
        LocalDateTime start = LocalDateTime.ofInstant(startTime.toInstant(), ZoneId.systemDefault());
        LocalDateTime end = start.plusMinutes(duration);
        LocalDateTime courseStart = LocalDateTime.ofInstant(dataBb.getSelectedCourse().getStart().toInstant(), ZoneId.systemDefault());
        LocalDateTime courseEnd = LocalDateTime.ofInstant(dataBb.getSelectedCourse().getEnd().toInstant(), ZoneId.systemDefault());
        if (start.compareTo(courseStart) < 0 || end.compareTo(courseEnd) > 0) {
            isAcceptableSlot = false;
        }
        return isAcceptableSlot;
    }

    public boolean checkOverlap(List<Slot> tmpSlotList, Date startTime, int duration, String room, Integer courseId) {
        boolean noOverlap = true;
        LocalDateTime start = LocalDateTime.ofInstant(startTime.toInstant(), ZoneId.systemDefault());
        LocalDateTime end = start.plusMinutes(duration);
        for (Slot s : tmpSlotList) {
            LocalDateTime slotstart = LocalDateTime.ofInstant(s.getStartdate().toInstant(), ZoneId.systemDefault());
            LocalDateTime slotend = slotstart.plusMinutes(duration);
            if ((start.compareTo(slotstart) > 0 && start.compareTo(slotend) < 0) || end.compareTo(slotstart) > 0 && end.compareTo(slotend) < 0) {
                if (s.getCourse().getCourseId().equals(courseId)) {
//                System.out.println("###### found overlap #######");
                    noOverlap = false;
                    break;
                } else if (s.getRoom().equals(room)) {
                    noOverlap = false;
                    break;
                }
            }
        }
        return noOverlap;
    }

    // ==================================
    // =          Getter & Setter       =
    // ==================================
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTeacherFirstName() {
        return teacherFirstName;
    }

    public void setTeacherFirstName(String teacherFirstName) {
        this.teacherFirstName = teacherFirstName;
    }

    public String getTeacherLastName() {
        return teacherLastName;
    }

    public void setTeacherLastName(String teacherLastName) {
        this.teacherLastName = teacherLastName;
    }

    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(String studentAddress) {
        this.studentAddress = studentAddress;
    }

    public String getStudentPostcode() {
        return studentPostcode;
    }

    public void setStudentPostcode(String studentPostcode) {
        this.studentPostcode = studentPostcode;
    }

    public String getStudentPosttown() {
        return studentPosttown;
    }

    public void setStudentPosttown(String studentPosttown) {
        this.studentPosttown = studentPosttown;
    }

    public String getStudentEMail() {
        return studentEMail;
    }

    public void setStudentEMail(String studentEMail) {
        this.studentEMail = studentEMail;
    }

    public String getStudentPhonenumber() {
        return studentPhonenumber;
    }

    public void setStudentPhonenumber(String studentPhonenumber) {
        this.studentPhonenumber = studentPhonenumber;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(String courseLevel) {
        this.courseLevel = courseLevel;
    }

    public String getCourseLanguage() {
        return courseLanguage;
    }

    public void setCourseLanguage(String courseLanguage) {
        this.courseLanguage = courseLanguage;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public Integer getResponsibleTeacherId() {
        return responsibleTeacherId;
    }

    public void setResponsibleTeacherId(Integer responsibleTeacherId) {
        this.responsibleTeacherId = responsibleTeacherId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public boolean isAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(boolean attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    public DataBB getDataBb() {
        return dataBb;
    }

    public void setDataBb(DataBB dataBb) {
        this.dataBb = dataBb;
    }

    public CasFactoryLocal getCasFactoryManager() {
        return casFactoryManager;
    }

    public void setCasFactoryManager(CasFactoryLocal casFactoryManager) {
        this.casFactoryManager = casFactoryManager;
    }

    public String getLogininfo() {
        return logininfo;
    }

    public void setLogininfo(String logininfo) {
        this.logininfo = logininfo;
    }

    public Integer getRegisterStudentId() {
        return registerStudentId;
    }

    public void setRegisterStudentId(Integer registerStudentId) {
        this.registerStudentId = registerStudentId;
    }

}
