/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.dao.SlotFacade;
import cas.entity.Slot;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class SlotLogic implements SlotLogicLocal {

    @EJB
    private SlotFacade slotDao;

    public SlotFacade getSlotDao() {
        return slotDao;
    }

    public void setSlotDao(SlotFacade slotDao) {
        this.slotDao = slotDao;
    }

    @Override
    public void addSlot(Slot slot) {
        getSlotDao().create(slot);
    }

    @Override
    public void updateSlot(Slot slot) {
        getSlotDao().edit(slot);
    }

    @Override
    public void deleteSlot(Integer id) {
        Slot ds = getSlotDao().find(id);
        getSlotDao().remove(ds);
    }

    @Override
    public Slot findSlot(Integer id) {
        return getSlotDao().find(id);
    }

    @Override
    public List<Slot> getAllSlot() {
        return getSlotDao().findAll();
    }

    @Override
    public List<Slot> getSortedSlotList(Integer id) {
        return getSlotDao().getSortedSlotList(id);
    }
}
