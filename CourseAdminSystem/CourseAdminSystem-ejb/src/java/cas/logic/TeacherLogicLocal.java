/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.entity.Teacher;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface TeacherLogicLocal {

    void addTeacher(Teacher t);

    Teacher findTeacher(Integer id);

    List<Teacher> getAllTeachers();

    void updateTeacher(Teacher uTeacher);

    void deleteTeacher(Integer id);

    List<Teacher> getSortedTeacherList();

}
