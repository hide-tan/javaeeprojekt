/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.entity.Slot;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface SlotLogicLocal {

    void addSlot(Slot slot);

    void updateSlot(Slot slot);

    void deleteSlot(Integer id);

    Slot findSlot(Integer id);

    List<Slot> getAllSlot();

    List<Slot> getSortedSlotList(Integer id);

}
