/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface CasFactoryLocal {

    AttendanceLogicLocal getAttendanceLogic();

    CourseLogicLocal getCourseLogic();

    SlotLogicLocal getSlotLogic();

    StudentLogicLocal getStudentLogic();

    TeacherLogicLocal getTeacherLogic();

    AdminLogicLocal getAdminLogic();

}
