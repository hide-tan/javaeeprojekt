/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.entity.Student;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface StudentLogicLocal {

    void addStudent(Student student);

    Student findStudent(Integer id);

    List<Student> getAllStudents();

    void updateStudent(Student uStudent);

    void deleteStudent(Integer id);

    List<Student> getSortedStudentList();

}
