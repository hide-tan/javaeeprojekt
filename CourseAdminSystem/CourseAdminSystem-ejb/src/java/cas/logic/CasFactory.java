/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class CasFactory implements CasFactoryLocal {

    @EJB
    private AttendanceLogicLocal attendance;
    @EJB
    private CourseLogicLocal course;
    @EJB
    private SlotLogicLocal slot;
    @EJB
    private StudentLogicLocal student;
    @EJB
    private TeacherLogicLocal teacher;
    @EJB
    private AdminLogicLocal admin;

    @Override
    public AttendanceLogicLocal getAttendanceLogic() {
        return attendance;
    }

    @Override
    public CourseLogicLocal getCourseLogic() {
        return course;
    }

    @Override
    public SlotLogicLocal getSlotLogic() {
        return slot;
    }

    @Override
    public StudentLogicLocal getStudentLogic() {
        return student;
    }

    @Override
    public TeacherLogicLocal getTeacherLogic() {
        return teacher;
    }

    @Override
    public AdminLogicLocal getAdminLogic() {
        return admin;
    }

}
