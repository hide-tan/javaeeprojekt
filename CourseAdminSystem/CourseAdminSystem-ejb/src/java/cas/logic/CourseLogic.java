/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.dao.CourseFacade;
import cas.entity.Course;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class CourseLogic implements CourseLogicLocal {

    @EJB
    private CourseFacade courceDao;

    public CourseFacade getCourceDao() {
        return courceDao;
    }

    public void setCourceDao(CourseFacade courceDao) {
        this.courceDao = courceDao;
    }

    @Override
    public void addCourse(Course course) {
        getCourceDao().create(course);
    }

    @Override
    public void updateCourse(Course course) {
        getCourceDao().edit(course);
    }

    @Override
    public Course findCourse(Integer courseId) {
        return getCourceDao().find(courseId);
    }

    @Override
    public List<Course> getAllCourse() {
        return getCourceDao().findAll();
    }

    @Override
    public void deleteCourse(Integer courseId) {
        Course dc = getCourceDao().find(courseId);
        getCourceDao().remove(dc);
    }

    @Override
    public List<Course> getSortedCourseList() {
        return getCourceDao().getSortedCourseList();
    }

}
