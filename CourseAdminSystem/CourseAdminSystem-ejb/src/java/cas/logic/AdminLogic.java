/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.dao.AdmingroupFacade;
import cas.dao.AdminuserFacade;
import cas.entity.Admingroup;
import cas.entity.AdmingroupPK;
import cas.entity.Adminuser;
import cas.util.SHA256Encoder;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class AdminLogic implements AdminLogicLocal {

    @EJB
    private AdminuserFacade adminuserDao;

    @EJB
    private AdmingroupFacade admingroupDao;

    public AdminuserFacade getAdminuserDao() {
        return adminuserDao;
    }

    public void setAdminuserDao(AdminuserFacade adminuserDao) {
        this.adminuserDao = adminuserDao;
    }

    public AdmingroupFacade getAdmingroupDao() {
        return admingroupDao;
    }

    public void setAdmingroupDao(AdmingroupFacade admingroupDao) {
        this.admingroupDao = admingroupDao;
    }

    @Override
    public void createUserAndGroup(String username, String password, String groupname) {
        Adminuser user = new Adminuser();
        user.setUsername(username);

        SHA256Encoder encoder = new SHA256Encoder();
        String encodedPassword = encoder.encodeString(password);
        user.setPassword(encodedPassword);

        Admingroup group = new Admingroup();
        group.setAdmingroupPK(new AdmingroupPK(username, groupname));
        group.setAdminuser(user);

        getAdminuserDao().create(user);
        getAdmingroupDao().create(group);
    }

    @Override
    public void removeUser(String username) {
        AdmingroupPK apk = new AdmingroupPK(username, "admin");
        Admingroup ag = getAdmingroupDao().find(apk);
        getAdmingroupDao().remove(ag);
        Adminuser user = getAdminuserDao().find(username);
        getAdminuserDao().remove(user);
    }

//    @Override
//    public List<Adminuser> getAllAdminusers() {
//        return getAdminuserDao().findAll();
//    }
    @Override
    public boolean checkDuplicate(String username) {
        Adminuser au = getAdminuserDao().find(username);
        if (au != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Adminuser> getSortedAdminuserList() {
        return getAdminuserDao().getSortedAdminuserList();
    }

}
