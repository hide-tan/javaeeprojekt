/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.entity.Course;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface CourseLogicLocal {

    void addCourse(Course course);

    void updateCourse(Course course);

    Course findCourse(Integer courseId);

    List<Course> getAllCourse();

    void deleteCourse(Integer courseId);

    List<Course> getSortedCourseList();

}
