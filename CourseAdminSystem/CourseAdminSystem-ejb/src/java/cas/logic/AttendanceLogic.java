/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.dao.AttendanceFacade;
import cas.entity.Attendance;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class AttendanceLogic implements AttendanceLogicLocal {

    @EJB
    private AttendanceFacade attendanceDao;

    public AttendanceFacade getAttendanceDao() {
        return attendanceDao;
    }

    public void setAttendanceDao(AttendanceFacade attendanceDao) {
        this.attendanceDao = attendanceDao;
    }

    @Override
    public void addAttendance(Attendance attendance) {
        getAttendanceDao().create(attendance);
    }

    @Override
    public void updateAttendance(Attendance attendance) {
        getAttendanceDao().edit(attendance);
    }

    @Override
    public void deleteAttendance(Integer id) {
        Attendance da = getAttendanceDao().find(id);
        getAttendanceDao().remove(da);
    }

    @Override
    public Attendance findAttendance(Integer id) {
        return getAttendanceDao().find(id);
    }

    @Override
    public List<Attendance> findAllAttendance() {
        return getAttendanceDao().findAll();
    }

    @Override
    public List<Attendance> getSortedAttendanceListBySlot(Integer id) {
        return getAttendanceDao().getSortedAttendanceListBySlot(id);
    }

    @Override
    public List<Attendance> getSortedAttendanceListByStudent(Integer id) {
        return getAttendanceDao().getSortedAttendanceListByStudent(id);
    }

}
