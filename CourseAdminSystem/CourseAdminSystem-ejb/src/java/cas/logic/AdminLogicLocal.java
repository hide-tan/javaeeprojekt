/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.entity.Adminuser;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface AdminLogicLocal {

    void createUserAndGroup(String username, String password, String groupname);

    void removeUser(String username);

//    List<Adminuser> getAllAdminusers();
    boolean checkDuplicate(String username);

    List<Adminuser> getSortedAdminuserList();
}
