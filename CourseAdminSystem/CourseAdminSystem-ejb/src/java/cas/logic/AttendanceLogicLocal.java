/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.entity.Attendance;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Local
public interface AttendanceLogicLocal {

    void addAttendance(Attendance attendance);

    void updateAttendance(Attendance attendance);

    void deleteAttendance(Integer id);

    Attendance findAttendance(Integer id);

    List<Attendance> findAllAttendance();

    List<Attendance> getSortedAttendanceListBySlot(Integer id);

    List<Attendance> getSortedAttendanceListByStudent(Integer id);

}
