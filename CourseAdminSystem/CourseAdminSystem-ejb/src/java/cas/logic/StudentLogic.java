/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.dao.StudentFacade;
import cas.entity.Student;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class StudentLogic implements StudentLogicLocal {

    @EJB
    private StudentFacade studentDao;

    public StudentFacade getStudentDao() {
        return studentDao;
    }

    public void setStudentDao(StudentFacade studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public void addStudent(Student student) {
        getStudentDao().create(student);
    }

    @Override
    public Student findStudent(Integer id) {
        return getStudentDao().find(id);
    }

    @Override
    public List<Student> getAllStudents() {
        return getStudentDao().findAll();
    }

    @Override
    public void updateStudent(Student uStudent) {
        getStudentDao().edit(uStudent);
    }

    @Override
    public void deleteStudent(Integer id) {
        Student dstu = getStudentDao().find(id);
        getStudentDao().remove(dstu);
    }

    @Override
    public List<Student> getSortedStudentList() {
        return getStudentDao().getSortedStudentList();
    }

}
