/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.logic;

import cas.dao.TeacherFacade;
import cas.entity.Teacher;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class TeacherLogic implements TeacherLogicLocal {

    @EJB
    private TeacherFacade teacherDao;

    public TeacherFacade getTeacherDao() {
        return teacherDao;
    }

    public void setTeacherDao(TeacherFacade teacherDao) {
        this.teacherDao = teacherDao;
    }

    @Override
    public void addTeacher(Teacher nt) {
//        Teacher nt = new Teacher(firstname, lastname);
        getTeacherDao().create(nt);
    }

    @Override
    public Teacher findTeacher(Integer id) {
        return getTeacherDao().find(id);
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return getTeacherDao().findAll();
    }

    @Override
    public void updateTeacher(Teacher uTeacher) {
//        Teacher ut = new Teacher(id, firstname, lastname);
        getTeacherDao().edit(uTeacher);
    }

    @Override
    public void deleteTeacher(Integer id) {
        Teacher dt = getTeacherDao().find(id);
        getTeacherDao().remove(dt);
    }

    @Override
    public List<Teacher> getSortedTeacherList() {
        return getTeacherDao().getSortedTeacherList();
    }

}
