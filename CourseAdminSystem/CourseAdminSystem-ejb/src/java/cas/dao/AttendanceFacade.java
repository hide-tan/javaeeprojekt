/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.dao;

import cas.entity.Attendance;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class AttendanceFacade extends AbstractFacade<Attendance> {

    @PersistenceContext(unitName = "CourseAdminSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AttendanceFacade() {
        super(Attendance.class);
    }

    public List<Attendance> getSortedAttendanceListBySlot(Integer id) {
        Query query = em.createNamedQuery("Attendance.findAllBySlot");
        query.setParameter("slotId", id);
        return query.getResultList();
    }

    public List<Attendance> getSortedAttendanceListByStudent(Integer id) {
        Query query = em.createNamedQuery("Attendance.findAllByStudent");
        query.setParameter("studentId", id);
        return query.getResultList();
    }

}
