/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.dao;

import cas.entity.Admingroup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class AdmingroupFacade extends AbstractFacade<Admingroup> {

    @PersistenceContext(unitName = "CourseAdminSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdmingroupFacade() {
        super(Admingroup.class);
    }

}
