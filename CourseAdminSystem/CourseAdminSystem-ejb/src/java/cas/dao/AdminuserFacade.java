/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.dao;

import cas.entity.Adminuser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class AdminuserFacade extends AbstractFacade<Adminuser> {

    @PersistenceContext(unitName = "CourseAdminSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminuserFacade() {
        super(Adminuser.class);
    }

    public List<Adminuser> getSortedAdminuserList() {
        Query query = em.createNamedQuery("Adminuser.findAll");
        return query.getResultList();
    }

}
