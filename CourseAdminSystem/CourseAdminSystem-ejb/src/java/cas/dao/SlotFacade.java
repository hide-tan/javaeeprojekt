/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.dao;

import cas.entity.Slot;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Stateless
public class SlotFacade extends AbstractFacade<Slot> {

    @PersistenceContext(unitName = "CourseAdminSystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SlotFacade() {
        super(Slot.class);
    }

    public List<Slot> getSortedSlotList(Integer id) {
        Query query = em.createNamedQuery("Slot.findAllByCourse");
        query.setParameter("courseId", id);
        return query.getResultList();
    }
}
