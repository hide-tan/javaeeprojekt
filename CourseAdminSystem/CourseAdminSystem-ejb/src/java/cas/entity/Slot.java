/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Entity
@Table(name = "Slot")
@NamedQueries({
    @NamedQuery(name = "Slot.findAll", query = "SELECT s FROM Slot s ORDER BY s.startdate ASC"),
    @NamedQuery(name = "Slot.findAllByCourse", query = "SELECT s FROM Slot s WHERE s.course.courseId = :courseId ORDER BY s.startdate ASC"),
    @NamedQuery(name = "Slot.findBySlotId", query = "SELECT s FROM Slot s WHERE s.slotId = :slotId"),
    @NamedQuery(name = "Slot.findByStartdate", query = "SELECT s FROM Slot s WHERE s.startdate = :startdate"),
    @NamedQuery(name = "Slot.findByDuration", query = "SELECT s FROM Slot s WHERE s.duration = :duration"),
    @NamedQuery(name = "Slot.findByRoom", query = "SELECT s FROM Slot s WHERE s.room = :room")})
public class Slot implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "slot_id")
    private Integer slotId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duration")
    private int duration;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "room")
    private String room;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "slot", fetch = FetchType.LAZY)
    private Collection<Attendance> attendanceCollection;
    @JoinColumn(name = "course_id", referencedColumnName = "course_id")
    @ManyToOne(optional = false)
    private Course course;

    public Slot() {
    }

    public Slot(Integer slotId) {
        this.slotId = slotId;
    }

    public Slot(Integer slotId, Date startdate, int duration, String room) {
        this.slotId = slotId;
        this.startdate = startdate;
        this.duration = duration;
        this.room = room;
    }

    public Integer getSlotId() {
        return slotId;
    }

    public void setSlotId(Integer slotId) {
        this.slotId = slotId;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Collection<Attendance> getAttendanceCollection() {
        return attendanceCollection;
    }

    public void setAttendanceCollection(Collection<Attendance> attendanceCollection) {
        this.attendanceCollection = attendanceCollection;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (slotId != null ? slotId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Slot)) {
            return false;
        }
        Slot other = (Slot) object;
        if ((this.slotId == null && other.slotId != null) || (this.slotId != null && !this.slotId.equals(other.slotId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cas.entity.Slot[ slotId=" + slotId + " ]";
        LocalDateTime slotStart = LocalDateTime.ofInstant(this.startdate.toInstant(), ZoneId.systemDefault());
        LocalDateTime slotEnd = slotStart.plusMinutes(duration);
        DateTimeFormatter dFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return this.slotId.toString() + ": " + slotStart.format(dFormat) + " - " + slotEnd.format(dFormat);
    }

}
