/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Entity
@Table(name = "Adminuser")
@NamedQueries({
    @NamedQuery(name = "Adminuser.findAll", query = "SELECT a FROM Adminuser a ORDER BY a.username ASC"),
    @NamedQuery(name = "Adminuser.findByUsername", query = "SELECT a FROM Adminuser a WHERE a.username = :username"),
    @NamedQuery(name = "Adminuser.findByPassword", query = "SELECT a FROM Adminuser a WHERE a.password = :password")})
public class Adminuser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "password")
    private String password;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "adminuser", fetch = FetchType.LAZY)
    private Collection<Admingroup> admingroupCollection;

    public Adminuser() {
    }

    public Adminuser(String username) {
        this.username = username;
    }

    public Adminuser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Admingroup> getAdmingroupCollection() {
        return admingroupCollection;
    }

    public void setAdmingroupCollection(Collection<Admingroup> admingroupCollection) {
        this.admingroupCollection = admingroupCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adminuser)) {
            return false;
        }
        Adminuser other = (Adminuser) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cas.entity.Adminuser[ username=" + username + " ]";
    }

}
