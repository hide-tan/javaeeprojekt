/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cas.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Hideyuki TANUSHI
 */
@Entity
@Table(name = "Admingroup")
@NamedQueries({
    @NamedQuery(name = "Admingroup.findAll", query = "SELECT a FROM Admingroup a"),
    @NamedQuery(name = "Admingroup.findByUsername", query = "SELECT a FROM Admingroup a WHERE a.admingroupPK.username = :username"),
    @NamedQuery(name = "Admingroup.findByGroupname", query = "SELECT a FROM Admingroup a WHERE a.admingroupPK.groupname = :groupname")})
public class Admingroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AdmingroupPK admingroupPK;
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Adminuser adminuser;

    public Admingroup() {
    }

    public Admingroup(AdmingroupPK admingroupPK) {
        this.admingroupPK = admingroupPK;
    }

    public Admingroup(String username, String groupname) {
        this.admingroupPK = new AdmingroupPK(username, groupname);
    }

    public AdmingroupPK getAdmingroupPK() {
        return admingroupPK;
    }

    public void setAdmingroupPK(AdmingroupPK admingroupPK) {
        this.admingroupPK = admingroupPK;
    }

    public Adminuser getAdminuser() {
        return adminuser;
    }

    public void setAdminuser(Adminuser adminuser) {
        this.adminuser = adminuser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (admingroupPK != null ? admingroupPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Admingroup)) {
            return false;
        }
        Admingroup other = (Admingroup) object;
        if ((this.admingroupPK == null && other.admingroupPK != null) || (this.admingroupPK != null && !this.admingroupPK.equals(other.admingroupPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cas.entity.Admingroup[ admingroupPK=" + admingroupPK + " ]";
    }

}
